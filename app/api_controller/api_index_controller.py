# coding: utf8
from tornado import gen

from app.base_controller import BaseController
from app.common import global_space
from app.models import Advertisement, Video, Tag, VideoType, PriceTypeDetail, VideoTime, VideoOrder, VideoCity, OperatingGuide
import logging as log
from django.db.models import Q
from libs.util import check_param

log.basicConfig(format='[%(asctime)s] [%(filename)s]:[line:%(lineno)d] [%(levelname)s] %(message)s', level=log.INFO)


# /api/banner
class Banner(BaseController):

    def get(self):
        data = []
        type = self.get_argument("type", 1)
        for advertisement in Advertisement.objects.filter(state=0).filter(category_id=type).order_by('show_order').all():
            data.append(advertisement.to_dict('/750_300'))
        self.render_json(data=data)


# /api/health-check
class Health(BaseController):

    def get(self):
        self.render_json(data={})


# /api/video-rel
class VideoRel(BaseController):
    def get(self):
        vt = VideoType.objects.filter(id=self.get_argument("id", '')).first()
        vo = Video.objects.filter(video_type=vt).select_related('video_type'). \
            select_related('video_order_type'). \
            prefetch_related('video_order_type__video_time'). \
            prefetch_related('tags'). \
            prefetch_related('video_order_type__price_type_detail')
        vo = vo.order_by('show_order', '-update_time')
        paginator = self.pagination(vo.all(), page_num=int(self.get_argument("pageNum", 1)), photo_style='/320_210')
        return self.render_json(data=paginator)


# /api/video-detail
class VideoDetail(BaseController):
    def get(self):
        vid = self.get_argument("id", '')
        if not vid:
            param = [vid, ]
            if check_param(param):
                self.render_json(code=1001, msg='参数不全')
                return
        vo = Video.objects.filter(id=vid).select_related('video_type'). \
            select_related('video_order_type'). \
            prefetch_related('tags'). \
            prefetch_related('video_order_type__price_type_detail').order_by('show_order'). \
            prefetch_related('video_order_type__price_type_detail__pricetype_set').order_by('show_order'). \
            prefetch_related('video_order_type__price_type_detail__pricetype_set__price_type_detail').order_by('show_order').first()
        return self.render_json(data=vo.to_dict(photo_style='/750_420', type=2))


# /api/video-play
class VideoPlay(BaseController):
    def get(self):
        vid = self.get_argument("id", '')
        if not vid:
            param = [vid, ]
            if check_param(param):
                self.render_json(code=1001, msg='参数不全')
                return
        vo = Video.objects.filter(id=vid).first()
        vo.play_time += 1
        vo.save()
        return self.render_json(data={'url': vo.url})


# /api/video-city
class City(BaseController):

    def get(self):
        return self.render_json(data=[key.to_dict() for key in VideoCity.objects.filter(state=0).all()])


# /api/video-list
class IndexVideos(BaseController):

    def get(self):
        tag_id = self.get_argument("tag", '')
        if tag_id:
            tag = Tag.objects.filter(id=tag_id).order_by('show_order').first()
        else:
            tag = Tag.objects.order_by('show_order').first()
        vo = tag.video_set.filter(state=0).select_related('video_type'). \
            select_related('video_order_type'). \
            prefetch_related('video_order_type__video_time'). \
            prefetch_related('tags'). \
            prefetch_related('video_order_type__price_type_detail')

        order = self.get_argument("order", '')
        if order and 'play_time' in order:
            vo = vo.order_by(order)
        elif 'price' in order:
            _list = [key.to_dict(photo_style='/710_476') for key in vo.all()]
            if order == 'price':
                _list.sort(key=lambda k: (k.get('transaction_price')))
            if order == '-price':
                _list.sort(key=lambda k: (-k.get('transaction_price')))
            paginator = self.pagination_list(object_list=_list, page_num=int(self.get_argument("pageNum", 1)))
            return self.render_json(data=paginator)
        else:
            vo = vo.order_by('show_order', '-update_time')

        paginator = self.pagination(vo.all(), page_num=int(self.get_argument("pageNum", 1)), photo_style='/710_476')
        return self.render_json(data=paginator)


# /api/video-search
class VideoSearch(BaseController):

    def get(self):
        vo = Video.objects.filter(state=0)
        title = self.get_argument("title", '')
        if title:
            vo = vo.filter(name__contains=title).select_related('video_type'). \
                select_related('video_order_type'). \
                prefetch_related('video_order_type__video_time'). \
                prefetch_related('tags'). \
                prefetch_related('video_order_type__price_type_detail')
        vo = vo.order_by('show_order', '-update_time')
        paginator = self.pagination(vo.all(), page_num=int(self.get_argument("pageNum", 1)), photo_style='/320_210')
        return self.render_json(data=paginator)


# /api/all-tags
class AllTags(BaseController):
    def get(self):
        vo = Tag.objects.filter(state=0)
        data = [key.to_dict() for key in vo.order_by('show_order').all()]
        self.render_json(data=data)


class Guide(BaseController):
    def get(self):
        vo = OperatingGuide.objects.filter(state=0)
        data = [key.to_dict() for key in vo.order_by('show_order').all()]
        self.render_json(data=data)


class GuideDetail(BaseController):
    @gen.coroutine
    def get(self):
        vo = OperatingGuide.objects.filter(state=0).filter(id=self.get_argument("id", '')).first()
        self.render_json(data=vo.to_dict())


class Citys(BaseController):
    def get(self):
        all = [{'name': '北京', 'first': 'B'},
               {'name': '上海', 'first': 'S'},
               {'name': '天津', 'first': 'T'},
               {'name': '重庆', 'first': 'C'},
               {'name': '阿克苏', 'first': 'A'},
               {'name': '安宁', 'first': 'A'},
               {'name': '安庆', 'first': 'A'},
               {'name': '鞍山', 'first': 'A'},
               {'name': '安顺', 'first': 'A'},
               {'name': '安阳', 'first': 'A'},
               {'name': '安康', 'first': 'A'},
               {'name': '包头', 'first': 'B'},
               {'name': '白城', 'first': 'B'},
               {'name': '白山', 'first': 'B'},
               {'name': '白银', 'first': 'B'},
               {'name': '蚌埠', 'first': 'B'},
               {'name': '保定', 'first': 'B'},
               {'name': '宝鸡', 'first': 'B'},
               {'name': '保山', 'first': 'B'},
               {'name': '巴中', 'first': 'B'},
               {'name': '北海', 'first': 'B'},
               {'name': '本溪', 'first': 'B'},
               {'name': '滨州', 'first': 'B'},
               {'name': '博乐', 'first': 'B'},
               {'name': '亳州', 'first': 'B'},
               {'name': '长春', 'first': 'C'},
               {'name': '长海', 'first': 'C'},
               {'name': '长乐', 'first': 'C'},
               {'name': '长沙', 'first': 'C'},
               {'name': '长治', 'first': 'C'},
               {'name': '沧州', 'first': 'C'},
               {'name': '常德', 'first': 'C'},
               {'name': '昌吉', 'first': 'C'},
               {'name': '常熟', 'first': 'C'},
               {'name': '常州', 'first': 'C'},
               {'name': '巢湖', 'first': 'C'},
               {'name': '朝阳', 'first': 'C'},
               {'name': '潮州', 'first': 'C'},
               {'name': '承德', 'first': 'C'},
               {'name': '成都', 'first': 'C'},
               {'name': '城固', 'first': 'C'},
               {'name': '郴州', 'first': 'C'},
               {'name': '赤壁', 'first': 'C'},
               {'name': '赤峰', 'first': 'C'},
               {'name': '赤水', 'first': 'C'},
               {'name': '池州', 'first': 'C'},
               {'name': '崇左', 'first': 'C'},
               {'name': '楚雄', 'first': 'C'},
               {'name': '滁州', 'first': 'C'},
               {'name': '慈溪', 'first': 'C'},
               {'name': '从化', 'first': 'C'},
               {'name': '大理', 'first': 'D'},
               {'name': '大连', 'first': 'D'},
               {'name': '丹东', 'first': 'D'},
               {'name': '丹阳', 'first': 'D'},
               {'name': '大庆', 'first': 'D'},
               {'name': '大同', 'first': 'D'},
               {'name': '达州', 'first': 'D'},
               {'name': '德阳', 'first': 'D'},
               {'name': '德州', 'first': 'D'},
               {'name': '东莞', 'first': 'D'},
               {'name': '东阳', 'first': 'D'},
               {'name': '东营', 'first': 'D'},
               {'name': '都匀', 'first': 'D'},
               {'name': '敦化', 'first': 'D'},
               {'name': '鄂尔多斯', 'first': 'E'},
               {'name': '恩施', 'first': 'E'},
               {'name': '鄂州', 'first': 'E'},
               {'name': '佛山', 'first': 'F'},
               {'name': '防城港', 'first': 'F'},
               {'name': '肥城', 'first': 'F'},
               {'name': '奉化', 'first': 'F'},
               {'name': '抚顺', 'first': 'F'},
               {'name': '阜新', 'first': 'F'},
               {'name': '阜阳', 'first': 'F'},
               {'name': '富阳', 'first': 'F'},
               {'name': '福州', 'first': 'F'},
               {'name': '抚州', 'first': 'F'},
               {'name': '福清', 'first': 'F'},
               {'name': '涪陵', 'first': 'F'},
               {'name': '赣榆', 'first': 'G'},
               {'name': '赣州', 'first': 'G'},
               {'name': '高明', 'first': 'G'},
               {'name': '高邮', 'first': 'G'},
               {'name': '格尔木', 'first': 'G'},
               {'name': '个旧', 'first': 'G'},
               {'name': '巩义', 'first': 'G'},
               {'name': '广安', 'first': 'G'},
               {'name': '广元', 'first': 'G'},
               {'name': '广州', 'first': 'G'},
               {'name': '古包头', 'first': 'G'},
               {'name': '贵港', 'first': 'G'},
               {'name': '桂林', 'first': 'G'},
               {'name': '贵阳', 'first': 'G'},
               {'name': '固原', 'first': 'G'},
               {'name': '公主岭', 'first': 'G'},
               {'name': '哈尔滨', 'first': 'H'},
               {'name': '海城', 'first': 'H'},
               {'name': '海口', 'first': 'H'},
               {'name': '海门', 'first': 'H'},
               {'name': '海宁', 'first': 'H'},
               {'name': '哈密', 'first': 'H'},
               {'name': '邯郸', 'first': 'H'},
               {'name': '杭州', 'first': 'H'},
               {'name': '汉中', 'first': 'H'},
               {'name': '鹤壁', 'first': 'H'},
               {'name': '合肥', 'first': 'H'},
               {'name': '衡水', 'first': 'H'},
               {'name': '衡阳', 'first': 'H'},
               {'name': '和田', 'first': 'H'},
               {'name': '河源', 'first': 'H'},
               {'name': '菏泽', 'first': 'H'},
               {'name': '花都', 'first': 'H'},
               {'name': '淮安', 'first': 'H'},
               {'name': '淮北', 'first': 'H'},
               {'name': '怀化', 'first': 'H'},
               {'name': '淮南', 'first': 'H'},
               {'name': '黄冈', 'first': 'H'},
               {'name': '黄山', 'first': 'H'},
               {'name': '黄石', 'first': 'H'},
               {'name': '呼和浩特', 'first': 'H'},
               {'name': '惠州', 'first': 'H'},
               {'name': '葫芦岛', 'first': 'H'},
               {'name': '湖州', 'first': 'H'},
               {'name': '佳木斯', 'first': 'J'},
               {'name': '吉安', 'first': 'J'},
               {'name': '江都', 'first': 'J'},
               {'name': '江门', 'first': 'J'},
               {'name': '江阴', 'first': 'J'},
               {'name': '胶南', 'first': 'J'},
               {'name': '胶州', 'first': 'J'},
               {'name': '焦作', 'first': 'J'},
               {'name': '嘉善', 'first': 'J'},
               {'name': '嘉兴', 'first': 'J'},
               {'name': '介休', 'first': 'J'},
               {'name': '吉林', 'first': 'J'},
               {'name': '即墨', 'first': 'J'},
               {'name': '济南', 'first': 'J'},
               {'name': '晋城', 'first': 'J'},
               {'name': '景德镇', 'first': 'J'},
               {'name': '景洪', 'first': 'J'},
               {'name': '靖江', 'first': 'J'},
               {'name': '荆门', 'first': 'J'},
               {'name': '荆州', 'first': 'J'},
               {'name': '金华', 'first': 'J'},
               {'name': '集宁', 'first': 'J'},
               {'name': '济宁', 'first': 'J'},
               {'name': '晋江', 'first': 'J'},
               {'name': '金坛', 'first': 'J'},
               {'name': '晋中', 'first': 'J'},
               {'name': '锦州', 'first': 'J'},
               {'name': '吉首', 'first': 'J'},
               {'name': '九江', 'first': 'J'},
               {'name': '酒泉', 'first': 'J'},
               {'name': '鸡西', 'first': 'J'},
               {'name': '济源', 'first': 'J'},
               {'name': '句容', 'first': 'J'},
               {'name': '江油', 'first': 'J'},
               {'name': '姜堰', 'first': 'J'},
               {'name': '开封', 'first': 'K'},
               {'name': '凯里', 'first': 'K'},
               {'name': '开平', 'first': 'K'},
               {'name': '开远', 'first': 'K'},
               {'name': '喀什', 'first': 'K'},
               {'name': '克拉玛依', 'first': 'K'},
               {'name': '库尔勒', 'first': 'K'},
               {'name': '奎屯', 'first': 'K'},
               {'name': '昆明', 'first': 'K'},
               {'name': '昆山', 'first': 'K'},
               {'name': '来宾', 'first': 'L'},
               {'name': '莱芜', 'first': 'L'},
               {'name': '莱西', 'first': 'L'},
               {'name': '莱州', 'first': 'L'},
               {'name': '廊坊', 'first': 'L'},
               {'name': '兰州', 'first': 'L'},
               {'name': '拉萨', 'first': 'L'},
               {'name': '乐山', 'first': 'L'},
               {'name': '连云港', 'first': 'L'},
               {'name': '聊城', 'first': 'L'},
               {'name': '辽阳', 'first': 'L'},
               {'name': '辽源', 'first': 'L'},
               {'name': '丽江', 'first': 'L'},
               {'name': '临安', 'first': 'L'},
               {'name': '临沧', 'first': 'L'},
               {'name': '临汾', 'first': 'L'},
               {'name': '灵宝', 'first': 'L'},
               {'name': '临河', 'first': 'L'},
               {'name': '临夏', 'first': 'L'},
               {'name': '临沂', 'first': 'L'},
               {'name': '丽水', 'first': 'L'},
               {'name': '六安', 'first': 'L'},
               {'name': '六盘水', 'first': 'L'},
               {'name': '柳州', 'first': 'L'},
               {'name': '溧阳', 'first': 'L'},
               {'name': '龙海', 'first': 'L'},
               {'name': '龙岩', 'first': 'L'},
               {'name': '娄底', 'first': 'L'},
               {'name': '漯河', 'first': 'L'},
               {'name': '洛阳', 'first': 'L'},
               {'name': '潞西', 'first': 'L'},
               {'name': '泸州', 'first': 'L'},
               {'name': '吕梁', 'first': 'L'},
               {'name': '旅顺', 'first': 'L'},
               {'name': '醴陵', 'first': 'L'},
               {'name': '马鞍山', 'first': 'M'},
               {'name': '茂名', 'first': 'M'},
               {'name': '梅河口', 'first': 'M'},
               {'name': '眉山', 'first': 'M'},
               {'name': '梅州', 'first': 'M'},
               {'name': '勉县', 'first': 'M'},
               {'name': '绵阳', 'first': 'M'},
               {'name': '牡丹江', 'first': 'M'},
               {'name': '南安', 'first': 'N'},
               {'name': '南昌', 'first': 'N'},
               {'name': '南充', 'first': 'N'},
               {'name': '南京', 'first': 'N'},
               {'name': '南宁', 'first': 'N'},
               {'name': '南平', 'first': 'N'},
               {'name': '南通', 'first': 'N'},
               {'name': '南阳', 'first': 'N'},
               {'name': '内江', 'first': 'N'},
               {'name': '宁波', 'first': 'N'},
               {'name': '宁德', 'first': 'N'},
               {'name': '盘锦', 'first': 'P'},
               {'name': '攀枝花', 'first': 'P'},
               {'name': '蓬莱', 'first': 'P'},
               {'name': '平顶山', 'first': 'P'},
               {'name': '平度', 'first': 'P'},
               {'name': '平湖', 'first': 'P'},
               {'name': '平凉', 'first': 'P'},
               {'name': '萍乡', 'first': 'P'},
               {'name': '普兰店', 'first': 'P'},
               {'name': '普宁', 'first': 'P'},
               {'name': '莆田', 'first': 'P'},
               {'name': '濮阳', 'first': 'P'},
               {'name': '黔南', 'first': 'Q'},
               {'name': '启东', 'first': 'Q'},
               {'name': '青岛', 'first': 'Q'},
               {'name': '庆阳', 'first': 'Q'},
               {'name': '清远', 'first': 'Q'},
               {'name': '青州', 'first': 'Q'},
               {'name': '秦皇岛', 'first': 'Q'},
               {'name': '钦州', 'first': 'Q'},
               {'name': '琼海', 'first': 'Q'},
               {'name': '齐齐哈尔', 'first': 'Q'},
               {'name': '泉州', 'first': 'Q'},
               {'name': '曲靖', 'first': 'Q'},
               {'name': '衢州', 'first': 'Q'},
               {'name': '七台河', 'first': 'Q'},
               {'name': '日喀则', 'first': 'R'},
               {'name': '日照', 'first': 'R'},
               {'name': '荣成', 'first': 'R'},
               {'name': '如皋', 'first': 'R'},
               {'name': '瑞安', 'first': 'R'},
               {'name': '乳山', 'first': 'R'},
               {'name': '三门峡', 'first': 'S'},
               {'name': '三明', 'first': 'S'},
               {'name': '三亚', 'first': 'S'},
               {'name': '佛山', 'first': 'S'},
               {'name': '商洛', 'first': 'S'},
               {'name': '商丘', 'first': 'S'},
               {'name': '上饶', 'first': 'S'},
               {'name': '上虞', 'first': 'S'},
               {'name': '汕头', 'first': 'S'},
               {'name': '韶关', 'first': 'S'},
               {'name': '绍兴', 'first': 'S'},
               {'name': '邵阳', 'first': 'S'},
               {'name': '沈阳', 'first': 'S'},
               {'name': '深圳', 'first': 'S'},
               {'name': '石河子', 'first': 'S'},
               {'name': '石家庄', 'first': 'S'},
               {'name': '石林', 'first': 'S'},
               {'name': '石狮', 'first': 'S'},
               {'name': '十堰', 'first': 'S'},
               {'name': '寿光', 'first': 'S'},
               {'name': '双鸭山', 'first': 'S'},
               {'name': '朔州', 'first': 'S'},
               {'name': '沭阳', 'first': 'S'},
               {'name': '思茅', 'first': 'S'},
               {'name': '四平', 'first': 'S'},
               {'name': '松原', 'first': 'S'},
               {'name': '遂宁', 'first': 'S'},
               {'name': '随州', 'first': 'S'},
               {'name': '宿迁', 'first': 'S'},
               {'name': '宿豫', 'first': 'S'},
               {'name': '宿州', 'first': 'S'},
               {'name': '苏州', 'first': 'S'},
               {'name': '邵武', 'first': 'S'},
               {'name': '松江', 'first': 'S'},
               {'name': '歙县', 'first': 'S'},
               {'name': '塔城', 'first': 'T'},
               {'name': '泰安', 'first': 'T'},
               {'name': '太仓', 'first': 'T'},
               {'name': '泰兴', 'first': 'T'},
               {'name': '太原', 'first': 'T'},
               {'name': '泰州', 'first': 'T'},
               {'name': '台州', 'first': 'T'},
               {'name': '唐山', 'first': 'T'},
               {'name': '腾冲', 'first': 'T'},
               {'name': '滕州', 'first': 'T'},
               {'name': '天门', 'first': 'T'},
               {'name': '天水', 'first': 'T'},
               {'name': '铁岭', 'first': 'T'},
               {'name': '铜川', 'first': 'T'},
               {'name': '通辽', 'first': 'T'},
               {'name': '铜陵', 'first': 'T'},
               {'name': '桐庐', 'first': 'T'},
               {'name': '铜仁', 'first': 'T'},
               {'name': '桐乡', 'first': 'T'},
               {'name': '通州', 'first': 'T'},
               {'name': '通化', 'first': 'T'},
               {'name': '吐鲁番', 'first': 'T'},
               {'name': '瓦房店', 'first': 'W'},
               {'name': '潍坊', 'first': 'W'},
               {'name': '威海', 'first': 'W'},
               {'name': '渭南', 'first': 'W'},
               {'name': '文登', 'first': 'W'},
               {'name': '温岭', 'first': 'W'},
               {'name': '温州', 'first': 'W'},
               {'name': '乌海', 'first': 'W'},
               {'name': '武汉', 'first': 'W'},
               {'name': '芜湖', 'first': 'W'},
               {'name': '吴江', 'first': 'W'},
               {'name': '乌兰浩特', 'first': 'W'},
               {'name': '武威', 'first': 'W'},
               {'name': '无锡', 'first': 'W'},
               {'name': '梧州', 'first': 'W'},
               {'name': '乌鲁木齐', 'first': 'W'},
               {'name': '万州', 'first': 'W'},
               {'name': '西安', 'first': 'X'},
               {'name': '项城', 'first': 'X'},
               {'name': '襄樊', 'first': 'X'},
               {'name': '香格里拉', 'first': 'X'},
               {'name': '象山', 'first': 'X'},
               {'name': '湘潭', 'first': 'X'},
               {'name': '湘乡', 'first': 'X'},
               {'name': '咸宁', 'first': 'X'},
               {'name': '厦门', 'first': 'X'},
               {'name': '仙桃', 'first': 'X'},
               {'name': '咸阳', 'first': 'X'},
               {'name': '西藏', 'first': 'X'},
               {'name': '西昌', 'first': 'X'},
               {'name': '邢台', 'first': 'X'},
               {'name': '兴义', 'first': 'X'},
               {'name': '西宁', 'first': 'X'},
               {'name': '新乡', 'first': 'X'},
               {'name': '信阳', 'first': 'X'},
               {'name': '新余', 'first': 'X'},
               {'name': '忻州', 'first': 'X'},
               {'name': '宣城', 'first': 'X'},
               {'name': '许昌', 'first': 'X'},
               {'name': '徐州', 'first': 'X'},
               {'name': '萧山', 'first': 'X'},
               {'name': '宣化', 'first': 'X'},
               {'name': '湘阴', 'first': 'X'},
               {'name': '兴化', 'first': 'X'},
               {'name': '辛集', 'first': 'X'},
               {'name': '雅安', 'first': 'Y'},
               {'name': '牙克石', 'first': 'Y'},
               {'name': '延安', 'first': 'Y'},
               {'name': '延边', 'first': 'Y'},
               {'name': '盐城', 'first': 'Y'},
               {'name': '阳江', 'first': 'Y'},
               {'name': '阳泉', 'first': 'Y'},
               {'name': '扬州', 'first': 'Y'},
               {'name': '延吉', 'first': 'Y'},
               {'name': '烟台', 'first': 'Y'},
               {'name': '兖州', 'first': 'Y'},
               {'name': '宜宾', 'first': 'Y'},
               {'name': '宜昌', 'first': 'Y'},
               {'name': '宜春', 'first': 'Y'},
               {'name': '伊春', 'first': 'Y'},
               {'name': '伊犁', 'first': 'Y'},
               {'name': '银川', 'first': 'Y'},
               {'name': '营口', 'first': 'Y'},
               {'name': '鹰潭', 'first': 'Y'},
               {'name': '伊宁', 'first': 'Y'},
               {'name': '义乌', 'first': 'Y'},
               {'name': '宜兴', 'first': 'Y'},
               {'name': '益阳', 'first': 'Y'},
               {'name': '永康', 'first': 'Y'},
               {'name': '永州', 'first': 'Y'},
               {'name': '岳阳', 'first': 'Y'},
               {'name': '玉环', 'first': 'Y'},
               {'name': '榆林', 'first': 'Y'},
               {'name': '玉林', 'first': 'Y'},
               {'name': '运城', 'first': 'Y'},
               {'name': '玉溪', 'first': 'Y'},
               {'name': '余姚', 'first': 'Y'},
               {'name': '枣庄', 'first': 'Z'},
               {'name': '增城', 'first': 'Z'},
               {'name': '张家港', 'first': 'Z'},
               {'name': '张家界', 'first': 'Z'},
               {'name': '张家口', 'first': 'Z'},
               {'name': '章丘', 'first': 'Z'},
               {'name': '张掖', 'first': 'Z'},
               {'name': '漳州', 'first': 'Z'},
               {'name': '湛江', 'first': 'Z'},
               {'name': '肇东', 'first': 'Z'},
               {'name': '肇庆', 'first': 'Z'},
               {'name': '昭通', 'first': 'Z'},
               {'name': '郑州', 'first': 'Z'},
               {'name': '镇江', 'first': 'Z'},
               {'name': '中山', 'first': 'Z'},
               {'name': '周口', 'first': 'Z'},
               {'name': '舟山', 'first': 'Z'},
               {'name': '诸城', 'first': 'Z'},
               {'name': '珠海', 'first': 'Z'},
               {'name': '诸暨', 'first': 'Z'},
               {'name': '驻马店', 'first': 'Z'},
               {'name': '株洲', 'first': 'Z'},
               {'name': '淄博', 'first': 'Z'},
               {'name': '自贡', 'first': 'Z'},
               {'name': '遵义', 'first': 'Z'},
               {'name': '邹城', 'first': 'Z'},
               {'name': '资阳', 'first': 'Z'}, ]
        hot = [{
            'name': '长沙', 'first': 'C',
        }]
        all.sort(key=take_first)
        self.render_json(data={
            'all': all,
            'hot': hot
        })


def take_first(elem):
    return elem['first']
