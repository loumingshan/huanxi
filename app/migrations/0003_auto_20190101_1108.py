# Generated by Django 2.1.2 on 2019-01-01 11:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20181231_1327'),
    ]

    operations = [
        migrations.CreateModel(
            name='VideoElement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_time', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('update_time', models.DateTimeField(auto_now=True, verbose_name='修改时间')),
                ('state', models.IntegerField(default=0)),
                ('show_order', models.IntegerField(default=0)),
                ('num', models.IntegerField(verbose_name='元素数量')),
                ('element', models.ForeignKey(blank=-1, default=-1, null=True, on_delete=django.db.models.deletion.SET_DEFAULT, to='app.Element', verbose_name='元素')),
            ],
            options={
                'db_table': 'video_element',
            },
        ),
        migrations.AddField(
            model_name='video',
            name='extra_price',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='videoelement',
            name='video',
            field=models.ForeignKey(blank=-1, default=-1, null=True, on_delete=django.db.models.deletion.SET_DEFAULT, to='app.Video', verbose_name='订单'),
        ),
        migrations.AddField(
            model_name='video',
            name='elements',
            field=models.ManyToManyField(through='app.VideoElement', to='app.Element'),
        ),
    ]
