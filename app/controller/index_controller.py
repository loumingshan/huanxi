# coding: utf8
from tornado import gen

from app.base_controller import BaseController
from app.common import global_space
from app.models import Advertisement, Operatingguide, VideoCity
import logging as log
from django.db.models import Q

from libs.address import province_list
from libs.util import check_param

log.basicConfig(format='[%(asctime)s] [%(filename)s]:[line:%(lineno)d] [%(levelname)s] %(message)s', level=log.INFO)


# /api/banner
class Banner(BaseController):

    def get(self):
        data = []
        type = self.get_argument("type", 1)
        for advertisement in Advertisement.objects.filter(state=0).filter(category_id=type).order_by('show_order').all():
            data.append(advertisement.to_dict('/750_300'))
        self.render_json(data=data)



#/api/video-city
class City(BaseController):
    def get(self):
        return self.render_json(data=province_list)


# /api/health-check
class Health(BaseController):

    def get(self):
        self.set_status(200)
        self.finish()
    #https的检查链接
    def head(self):
        self.set_status(200)
        self.finish()


class Guide(BaseController):
    def get(self):
        vo = Operatingguide.objects.filter(state=0)
        data = [key.to_dict() for key in vo.order_by('show_order').all()]
        self.render_json(data=data)

class GuideDetail(BaseController):
    @gen.coroutine
    def get(self):
        vo = Operatingguide.objects.filter(state=0).filter(id=self.get_argument("id", '')).first()
        self.render_json(data=vo.to_dict())

