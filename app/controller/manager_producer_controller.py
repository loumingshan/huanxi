# -*- coding: utf-8 -*-
import logging as log
import uuid

import tornado

import cms_config
from app.base_controller import BaseController
from app.common import global_space
from app.models import Team, VideoOrder, OrderTeam, Producer, OrderProducer, User
from libs.util import check_param
from datetime import datetime

log.basicConfig(format='[%(asctime)s] [%(filename)s]:[line:%(lineno)d] [%(levelname)s] %(message)s', level=log.INFO)


# 团队列表
class TeamList(BaseController):

    def get(self):
        session_id = self.get_argument("session_id", '')
        if check_param([session_id]):
            return self.render_json(code=1001, msg='参数不全')
        session = global_space.very_session(session_id)
        if not session or 'id' not in session:
            return self.render_json(code=999, msg='session 失效')

        t = Team.objects.order_by('-score')
        paginator = self.pagination(t.all(), page_num=int(self.get_argument("pageNum", 1)))
        return self.render_json(data=paginator)


class SubmitPhone(BaseController):

    def post(self):
        session_id = self.get_json_argument("session_id", '')
        phone = self.get_json_argument("phone", '')
        if check_param([session_id, phone]):
            return self.render_json(code=1001, msg='参数不全')
        session = global_space.very_session(session_id)
        if not session or 'id' not in session:
            return self.render_json(code=999, msg='session 失效')

        User.objects.filter(id=session['id']).update(phone=phone)
        return self.render_json(msg='提交成功')


class TeamDetail(BaseController):

    def get(self):
        session_id = self.get_argument("session_id", '')
        team_id = self.get_argument('team_id', '')
        if check_param([session_id, team_id]):
            return self.render_json(code=1001, msg='参数不全')
        session = global_space.very_session(session_id)
        if not session or 'id' not in session:
            return self.render_json(code=999, msg='session 失效')

        team = Team.objects.filter(id=team_id).first()
        if not team:
            return self.render_json(code=999, msg='团队不存在')            
        
        return self.render_json(data=team.to_detail_dict())


class TeamOrderCommentList(BaseController):

    def get(self):
        session_id = self.get_argument("session_id", '')
        team_id = self.get_argument('team_id', '')
        if check_param([session_id, team_id]):
            return self.render_json(code=1001, msg='参数不全')
        session = global_space.very_session(session_id)
        if not session or 'id' not in session:
            return self.render_json(code=999, msg='session 失效')

        ot = OrderTeam.objects.filter(team_id=team_id).filter(order_status=OrderTeam.WC).filter(state=0).order_by('-create_time')
        paginator = self.pagination_list(ot.all(), page_num=int(self.get_argument("pageNum", 1)))  
        data = []
        for x in paginator['records']:
            data.append(x.to_comment_dict())
        paginator['records'] = data
        return self.render_json(data=paginator)


class TeamAcceptOrder(BaseController):

    def post(self):
        session_id = self.get_json_argument("session_id", '')
        order_id = self.get_json_argument('order_id', '')
        team_id = self.get_json_argument('team_id', '')
        if check_param([session_id, order_id, team_id]):
            return self.render_json(code=1001, msg='参数不全')
        session = global_space.very_session(session_id)
        if not session or 'id' not in session:
            return self.render_json(code=999, msg='session 失效')

        team = Team.objects.filter(id = team_id).first()
        if not team:
            return self.render_json(code=999, msg='团队不存在')            
        video_order = VideoOrder.objects.filter(order_id=order_id).first()
        if not video_order:
            return self.render_json(code=999, msg='订单不存在')            
        order_team = OrderTeam.objects.filter(order=video_order).filter(state=0).first()
        if not order_team:
            return self.render_json(code=999, msg='异常操作')

        order_team.team = team
        order_team.order_status = OrderTeam.BJ
        order_team.save()

        video_order.state = VideoOrder.STATE_PENDING_START
        video_order.save()
        return self.render_json(msg="保存成功")


class TeamProducerDetail(BaseController):

    def get(self):
        session_id = self.get_argument("session_id", '')
        team_id = self.get_argument('team_id', '')
        producer_id = self.get_argument('producer_id', '')
        if check_param([session_id, team_id, producer_id]):
            return self.render_json(code=1001, msg='参数不全')
        session = global_space.very_session(session_id)
        if not session or 'id' not in session:
            return self.render_json(code=999, msg='session 失效')

        team = Team.objects.filter(id=team_id).first()
        if not team:
            return self.render_json(code=999, msg='团队不存在') 
        producer = Producer.objects.filter(id=producer_id).first()
        if not producer:
            return self.render_json(code=999, msg='成员不存在')          
        data = producer.to_dict()
        print(data, team, team.get_owner_phone)
        data['team_owner_phone'] = team.get_owner_phone
        
        return self.render_json(data=data)


class ProducerOrderCommentList(BaseController):

    def get(self):
        session_id = self.get_argument("session_id", '')
        producer_id = self.get_argument('producer_id', '')
        if check_param([session_id, producer_id]):
            return self.render_json(code=1001, msg='参数不全')
        session = global_space.very_session(session_id)
        if not session or 'id' not in session:
            return self.render_json(code=999, msg='session 失效')

        op = OrderProducer.objects.filter(producer_id=producer_id).filter(order_status=OrderProducer.WC).filter(state=0).order_by('-create_time')
        paginator = self.pagination_list(op.all(), page_num=int(self.get_argument("pageNum", 1)))  
        data = []
        for x in paginator['records']:
            data.append(x.to_comment_dict())
        paginator['records'] = data
        return self.render_json(data=paginator)


class ProducerList(BaseController):

    def get(self):
        """推荐人列表
        """
        session_id = self.get_argument("session_id", '')
        if check_param([session_id]):
            return self.render_json(code=1001, msg='参数不全')
        session = global_space.very_session(session_id)
        if not session or 'id' not in session:
            return self.render_json(code=999, msg='session 失效')
        p = Producer.objects
        element_id = self.get_argument('element_id', '')
        if element_id:
            p = p.filter(elements__id=element_id)
        p = p.order_by('-score')
        paginator = self.pagination(p.all(), page_num=int(self.get_argument("pageNum", 1)))
        return self.render_json(data=paginator)


class ProducerDetail(BaseController):

    def get(self):
        """推荐人列表
        """
        session_id = self.get_argument("session_id", '')
        producer_id = self.get_argument("producer_id", '')
        if check_param([session_id, producer_id]):
            return self.render_json(code=1001, msg='参数不全')
        session = global_space.very_session(session_id)
        if not session or 'id' not in session:
            return self.render_json(code=999, msg='session 失效')
        p = Producer.objects.filter(id=producer_id).first()
        if not p:
            return self.render_json(code=999, msg='生产者不存在')
        return self.render_json(data=p.to_dict())
