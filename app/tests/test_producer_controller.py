# -*- coding: utf-8 -*-
from controller_test_case import ControllerTestCase
from libs import myjson as json
import unittest
import sys


class ProducerControllerTest(ControllerTestCase):

    def test_a_should_give_me_ten_producer(self):
        """测试普通用户列表，返回19个普通用户
        """
        res = self.get('/api/producer/order_detail', data={'id': 10, 'session_id': '3280ff240c1411e9b3e700163e020b4e'})
        self.assertEqual(200, res.code)
        d = json.loads(res.body)
        self.assertEqual(0, d['code'])
        self.assertEqual('2018-12-31', d['data']['plan_start_time'])

    def test_b_update_order_team_time(self):
        """更新整包订单时间
        """
        res = self.post('/api/orderteam/update-time', data={'id': 10, 'session_id': '3280ff240c1411e9b3e700163e020b4e', 
            'key': 'plan_end_time', 'value': '2019-01-28', 'order_no': 'fb54260c8ea6351953ef70946af629f5'})
        self.assertEqual(200, res.code)
        d = json.loads(res.body)
        self.assertEqual('更新成功', d['msg'])

    def test_c_should_give_me_error_update_order_team_time(self):
        """更新整包订单时间
        """
        res = self.post('/api/orderteam/update-time', data={'id': 10, 'session_id': '3280ff240c1411e9b3e700163e020b4e', 
            'key': 'plan_start_time', 'value': '2018-01-28', 'order_no': 'fb54260c8ea6351953ef70946af629f5'})
        self.assertEqual(200, res.code)
        d = json.loads(res.body)
        print(d)
        self.assertEqual(1001, d['code'])

    def test_d_update_user_phone(self):
        res = self.post('/api/manager/producer/submit-phone', data={'session_id': '3280ff240c1411e9b3e700163e020b4e', 
            'phone': '15084802204'})
        self.assertEqual(200, res.code)
        d = json.loads(res.body)
        print(d)
        self.assertEqual('提交成功', d['msg'])        

if __name__ == '__main__':
    sys.argv = sys.argv[:1]
    unittest.main()