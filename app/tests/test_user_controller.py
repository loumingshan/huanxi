# -*- coding: utf-8 -*-
from controller_test_case import ControllerTestCase
from libs import myjson as json
import unittest
import sys


class ProducerControllerTest(ControllerTestCase):

    def test_a_submit_team_applications(self):
        """测试申请入驻团队
        """
        res = self.post('/api/user/submit-applications', data={'type': 1, 'phone': '15084802204', 'session_id': 'f5da812b63eb475e80d54aa21cbdb024', 'team_name': '测试团队哦'})
        self.assertEqual(200, res.code)
        d = json.loads(res.body)
        print(d)
      
    def test_b_submit_person_applications(self):
        """测试申请入驻团队
        """
        res = self.post('/api/user/submit-applications', data={'type': 2, 'phone': '15084802204', 'session_id': '3280ff240c1411e9b3e700163e020b4e', 'jobs': '1,3,5'})
        self.assertEqual(200, res.code)
        d = json.loads(res.body)
        print(d)

if __name__ == '__main__':
    sys.argv = sys.argv[:1]
    unittest.main()