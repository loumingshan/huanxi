#coding: utf8
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))

from settings import ENV

if ENV.lower() != 'test':
    print ('just support run tests in test ENV')
    sys.exit(-1)
