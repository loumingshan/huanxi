def get_price_and_transtrant_price(video_duration, price_type_detail):
    director = 0
    shoot_team = 0
    equipment = 0
    quality = 0
    production = 0
    shoot_day = 0
    plot_team = 0

    director_transaction = 0
    shoot_team_transaction = 0
    equipment_transaction = 0
    quality_transaction = 0
    production_transaction = 0
    shoot_day_transaction = 0
    plot_team_transaction = 0

    for detail in price_type_detail:
        if detail.pricetype_set.first().id == 1:
            plot_team = detail.price
            plot_team_transaction = detail.transaction_price
        elif detail.pricetype_set.first().id == 2:
            director = detail.price
            director_transaction = detail.transaction_price
        elif detail.pricetype_set.first().id == 3:
            shoot_team = detail.price
            shoot_team_transaction = detail.transaction_price
        elif detail.pricetype_set.first().id == 4:
            quality = detail.price
            quality_transaction = detail.transaction_price
        elif detail.pricetype_set.first().id == 5:
            equipment = detail.price
            equipment_transaction = detail.transaction_price
        elif detail.pricetype_set.first().id == 6:
            production = detail.price
            production_transaction = detail.transaction_price
        elif detail.pricetype_set.first().id == 7:
            shoot_day = detail.price
            shoot_day_transaction = detail.transaction_price

    final_price = (director + shoot_team + equipment) * shoot_day + plot_team + quality + video_duration * production
    final_transaction_price = (director_transaction + shoot_team_transaction + equipment_transaction) * shoot_day_transaction \
                              + plot_team_transaction + quality_transaction + video_duration * production_transaction
    return final_price, final_transaction_price
