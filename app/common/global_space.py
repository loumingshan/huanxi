# coding: utf8
import redis

import cms_config
from libs import myjson as json
import logging as log
import settings

log.basicConfig(format='[%(asctime)s] [%(filename)s]:[line:%(lineno)d] [%(levelname)s] %(message)s', level=log.INFO)
# pool = redis.ConnectionPool(host=cms_config.redis_host, password=cms_config.redis_pass, port=6379, db=0, max_connections=200)

redis_host = '127.0.0.1'
redis_port = '6379'
redis_password = ''
if settings.ENV == 'aliyun':
    redis_host = cms_config.redis_host
    redis_password = cms_config.redis_pass

pool = redis.ConnectionPool(host=redis_host, port=redis_port, db=0, password=redis_password, max_connections=200)


def set_session(key, value, time):
    r = redis.Redis(connection_pool=pool)
    r.set(key, json.dumps(value), ex=time)


def very_session(key):
    r = redis.Redis(connection_pool=pool)
    if r.get(key):
        return json.loads(r.get(key))
    else:
        return None


def set_value(key, value, time=None):
    r = redis.Redis(connection_pool=pool)
    r.set(key, json.dumps(value), ex=time)


def get_value(key):
    r = redis.Redis(connection_pool=pool)
    return r.get(key)


def get_order_id():
    r = redis.Redis(connection_pool=pool)
    return r.incr('ORDER_ID_', 1)
