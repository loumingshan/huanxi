from tornado.web import url

from app.controller import index_controller,user_controller,producer_controller,video_controller,goods_controller
from manager_urls import manager_urls


urls = [



    url(r'/api/login', user_controller.Login),
    url(r'/api/user/submit-applications', user_controller.SubmitEnterApplications),
    url(r'/api/producer/new_order_list', producer_controller.NewProducerOrders),
    url(r'/api/producer/run_order_list', producer_controller.RuningProducerOrders),
    url(r'/api/producer/finish_order_list', producer_controller.FinishProducerOrders),
    url(r'/api/producer/get_order', producer_controller.GetOrder),
    url(r'/api/producer/order_detail', producer_controller.OrderDetail),
    url(r'/api/producer/change_order', producer_controller.ChangeOrderStatus),
    url(r'/api/producer/order_static', producer_controller.StaticProducerOrders),
    url(r'/api/orderteam/update-time', producer_controller.UpdateOrderTeamTime),

    url(r'/api/video/search', video_controller.SearchVideos),
    url(r'/api/video/list', video_controller.IndexVideos),
    url(r'/api/video/play', video_controller.VideoPlay),
    url(r'/api/video/detail', video_controller.VideoDetail),
    url(r'/api/index/recommend', video_controller.AllRecommend),
    url(r'/api/index/tags', video_controller.AllTags),
    url(r'/api/index/serie', video_controller.SerieList),
    url(r'/api/index/create_order', video_controller.CreateOrderByVideo),
    url(r'/api/index/to_pay', video_controller.ToPayByVideo),

    url(r'/api/video-guide', index_controller.Guide),
    url(r'/api/video-guide-detail', index_controller.GuideDetail),
    url(r'/api/video-city', index_controller.City),
    url(r'/api/video-type', video_controller.VideoTypeList),
    url(r'/api/video-type-options', video_controller.CategoryList),
    url(r'/api/video-order-list', video_controller.OrderList),
    url(r'/api/video-cart-list', video_controller.CartList),
    url(r'/api/video-order-detail', video_controller.OrderDetail),
    url(r'/api/create_custom_cart', video_controller.CreateOrderCustomToCart),
    url(r'/api/create_custom_order', video_controller.CreateOrderCustom),
    url(r'/api/create_video_cart', video_controller.CreateOrderByVideoToCart),
    # url(r'/api/video-cart-list', video_controller.CartList),
    url(r'/api/video-order-accept', video_controller.ToFinish),
    url(r'/api/video-order-cancel', video_controller.OrderCancel),
    url(r'/api/video-order-notify-second', video_controller.PayNotifySecond),
    url(r'/api/order-totoal-num', video_controller.OrderTotal),
    url(r'/api/order-refund', video_controller.OrderRefund),

    # url(r'/login', user_controller.Login),
    # url(r'/', index_controller.Index),
    # url(r'/api/login', api_user_controller.Login),
    # url(r'/api/user-contact', api_user_controller.ContactInfo),
    # 获取banner
    url(r'/api/banner', index_controller.Banner),
    # 获取视频列表，可以根据tag_id来查询
    # url(r'/api/video-list', api_index_controller.IndexVideos),
    # url(r'/api/video-guide', api_index_controller.Guide),
    # url(r'/api/video-guide-detail', api_index_controller.GuideDetail),
    # url(r'/api/video-city', api_index_controller.City),
    # url(r'/api/all-city', api_index_controller.Citys),

    # # 视频详情，根据视频id
    # url(r'/api/video-detail', api_index_controller.VideoDetail),
    # # 视频查询，根据title
    # url(r'/api/video-search', api_index_controller.VideoSearch),
    # url(r'/api/video-rel', api_index_controller.VideoRel),
    # url(r'/api/video-play', api_index_controller.VideoPlay),
    # url(r'/api/video-order-create', api_video_controller.VideoOrderCreate),
    # url(r'/api/video-order-modify', api_video_controller.VideoOrderModify),
    # url(r'/api/video-order-detail', api_video_controller.VideoOrderDetail),
    # url(r'/api/video-order-list', api_video_controller.VideoOrderList),
    # url(r'/api/video-order-count', api_video_controller.VideoOrderCount),
    # url(r'/api/video-order-pay', api_video_controller.Pay),
    # url(r'/api/video-price-detail', api_video_controller.PriceDetail),
    # url(r'/api/video-order-confirm', api_video_controller.VideoOrderConfirm),
    # url(r'/api/video-order-status-change', api_video_controller.ChangeVideoStatus),
    # url(r'/api/video-order-comments', api_video_controller.Comments),
    url(r'/api/video-order-notify', video_controller.PayNotify),
    url(r'/api/goods-list', goods_controller.GoodList),
    url(r'/api/goods-category', goods_controller.GoodCategory),
    url(r'/api/goods-order-notify', goods_controller.PayNotify),
    url(r'/api/goods-order-pay', goods_controller.Pay),
    url(r'/api/goods-order-close', goods_controller.OrderClose),
    url(r'/api/goods-order-detail', goods_controller.Detail),
    url(r'/api/goods-order-list', goods_controller.List),
    url(r'/api/goods-order-comment', goods_controller.Comments),
    #
    url(r'/api/goods-order-create', goods_controller.OrderCreate),
    url(r'/api/goods-order-confirm', goods_controller.OrderConfirm),
    url(r'/api/goods-detail', goods_controller.GoodDetail),
    url(r'/api/goods-comment', goods_controller.CommentList),
    url(r'/api/goods-top', goods_controller.GoodTop),
    # # 获取所有标签
    # url(r'/api/all-tags', api_index_controller.AllTags),
    # # 系统健康检查使用
    # url(r'/api/health-check', api_index_controller.Health),
    # # 拍片获取视频类型
    # url(r'/api/video-type', api_video_controller.VideoTypeList),
    # # 拍片获取视频时长
    # url(r'/api/video-time', api_video_controller.VideoTimeList),
    # # 获取所有视频拍摄因素
    # url(r'/api/video-price', api_video_controller.VideoPrice),
    # #
    # url(r'/api/feedback', api_goods_controller.FeedBack),
    #
]

urls = urls + manager_urls
