from tornado.web import url

from app.controller import manager_order_controller
from app.controller import manager_producer_controller

manager_urls = [

    url(r'/api/manager/order/summary', manager_order_controller.OrderSummary),
    url(r'/api/manager/order/list', manager_order_controller.OrderList),
    url(r'/api/manager/order/detail', manager_order_controller.OrderDetail),
    url(r'/api/manager/order/change-demand', manager_order_controller.ChangeDemand),
    url(r'/api/manager/order/demand-detail', manager_order_controller.OrderRequirementDetail),
    url(r'/api/manager/order/modify-price', manager_order_controller.ModifyPrice),
    url(r'/api/manager/order/pending-info', manager_order_controller.OrderPendingDetail),
    url(r'/api/manager/order/modify-pending-info', manager_order_controller.ModifyPendingInfo),
    url(r'/api/manager/order/modify-producer-type', manager_order_controller.ModifyProducerType),
    url(r'/api/manager/order/auto-match', manager_order_controller.ModifyAutoSendOrder),
    url(r'/api/manager/order/change-producer', manager_order_controller.ModifyOrderProducer),
    url(r'/api/manager/order/change-team', manager_order_controller.ModifyOrderTeam),
    url(r'/api/manager/order/producers-comment', manager_order_controller.OrderProducerComments),
    url(r'/api/manager/order/team-comment', manager_order_controller.OrderTeamComments),
    url(r'/api/manager/order/submit-producer-comment', manager_order_controller.SubmitOrderProducerComment),
    url(r'/api/manager/order/submit-team-comment', manager_order_controller.SubmitTeamComment),
    url(r'/api/manager/order/modify-status', manager_order_controller.ModifyOrderStatus),
    url(r'/api/manager/order/modify-rate', manager_order_controller.ModifyOrderCommission),
    url(r'/api/manager/order/pick-up', manager_order_controller.PickupOrder),
    url(r'/api/manager/order/count', manager_order_controller.OrderCount),

    url(r'/api/manager/team/list', manager_producer_controller.TeamList),
    url(r'/api/manager/team/detail', manager_producer_controller.TeamDetail),
    url(r'/api/manager/team/comment-list', manager_producer_controller.TeamOrderCommentList),
    url(r'/api/manager/team/producer-detail', manager_producer_controller.TeamProducerDetail),

    url(r'/api/manager/producer/submit-phone', manager_producer_controller.SubmitPhone),
    url(r'/api/manager/producer/list', manager_producer_controller.ProducerList),
    url(r'/api/manager/producer/detail', manager_producer_controller.ProducerDetail),
    url(r'/api/manager/producer/comment-list', manager_producer_controller.ProducerOrderCommentList),
]